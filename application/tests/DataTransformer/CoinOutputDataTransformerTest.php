<?php

namespace App\Tests\DataTransformer;

use App\DataTransformer\CoinOutputDataTransformer;
use App\Dto\CoinOutput;
use App\Entity\Coin;
use App\Entity\User;
use App\Transaction\TransactionFeeCalculatorInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @covers \App\DataTransformer\CoinOutputDataTransformer
 * @covers \App\Dto\CoinOutput
 */
class CoinOutputDataTransformerTest extends TestCase
{
    public function testTransform(): void
    {
        $user = new User();
        $coin = new Coin('TEST', 'TEST', 1);
        $fee = 0.2;

        $security = $this->createMock(Security::class);
        $security->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $transactionFeeCalculator = $this->createMock(TransactionFeeCalculatorInterface::class);
        $transactionFeeCalculator->expects($this->once())
            ->method('calculate')
            ->with($coin, $user, 1)
            ->willReturn($fee);

        $transformer = new CoinOutputDataTransformer($security, $transactionFeeCalculator);

        $output = $transformer->transform($coin, CoinOutput::class);

        $this->assertEquals($coin->getCode(), $output->code);
        $this->assertEquals($coin->getName(), $output->name);
        $this->assertEquals($coin->getPrice(), $output->price);
        $this->assertEquals($coin->getPrice() + $fee, $output->buyPrice);
        $this->assertEquals($coin->getPrice() - $fee, $output->sellPrice);
    }

    public function testTransformInvalidUser(): void
    {
        $user = $this->createMock(UserInterface::class);
        $coin = new Coin('TEST', 'TEST', 1);
        $fee = 0.2;

        $security = $this->createMock(Security::class);
        $security->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $transactionFeeCalculator = $this->createMock(TransactionFeeCalculatorInterface::class);
        $transactionFeeCalculator->expects($this->once())
            ->method('calculate')
            ->with($coin, null, 1)
            ->willReturn($fee);

        $transformer = new CoinOutputDataTransformer($security, $transactionFeeCalculator);

        $transformer->transform($coin, CoinOutput::class);

        $this->doesNotPerformAssertions();
    }

    public function testSupportsTransformation(): void
    {
        $security = $this->createMock(Security::class);
        $transactionFeeCalculator = $this->createMock(TransactionFeeCalculatorInterface::class);

        $transformer = new CoinOutputDataTransformer($security, $transactionFeeCalculator);
        $coin = new Coin('TEST', 'TEST', 1);

        $this->assertTrue($transformer->supportsTransformation($coin, CoinOutput::class));
        $this->assertFalse($transformer->supportsTransformation(new \stdClass(), CoinOutput::class));
        $this->assertFalse($transformer->supportsTransformation($coin, 'test'));
    }
}
