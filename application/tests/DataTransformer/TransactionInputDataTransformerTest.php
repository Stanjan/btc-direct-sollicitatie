<?php

namespace App\Tests\DataTransformer;

use App\DataTransformer\TransactionInputDataTransformer;
use App\Dto\TransactionInput;
use App\Entity\Coin;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\CoinRepository;
use App\Transaction\TransactionFactoryInterface;
use App\Transaction\TransactionType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * @covers \App\DataTransformer\TransactionInputDataTransformer
 * @covers \App\Dto\TransactionInput
 */
class TransactionInputDataTransformerTest extends TestCase
{
    public function testTransform(): void
    {
        $user = new User();
        $coin = new Coin('TEST', 'TEST', 1);
        $type = TransactionType::Buy;
        $quantity = 2;
        $transaction = new Transaction($user, $coin, $type, $quantity, 0, 0);
        $input = new TransactionInput($coin->getCode(), $type->value, $quantity);

        $security = $this->createMock(Security::class);
        $security->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $coinRepository = $this->createMock(CoinRepository::class);
        $coinRepository->expects($this->once())
            ->method('findOneBy')
            ->with(['code' => $input->coinCode])
            ->willReturn($coin);

        $transactionFactory = $this->createMock(TransactionFactoryInterface::class);
        $transactionFactory->expects($this->once())
            ->method('create')
            ->with($coin, $user, $type, $quantity)
            ->willReturn($transaction);

        $transformer = new TransactionInputDataTransformer($security, $coinRepository, $transactionFactory);

        $newTransaction = $transformer->transform($input, Transaction::class);

        $this->assertSame($transaction, $newTransaction);
    }

    public function testTransformWithoutUser(): void
    {
        $input = new TransactionInput('TEST', TransactionType::Buy->value, 2);

        $security = $this->createMock(Security::class);
        $security->expects($this->once())
            ->method('getUser')
            ->willReturn(null);
        $coinRepository = $this->createMock(CoinRepository::class);
        $transactionFactory = $this->createMock(TransactionFactoryInterface::class);

        $transformer = new TransactionInputDataTransformer($security, $coinRepository, $transactionFactory);

        $this->expectException(AccessDeniedHttpException::class);
        $this->expectExceptionMessage('User must be an instance of \App\Entity\User');

        $transformer->transform($input, Transaction::class);
    }

    public function testTransformInvalidTransactionType(): void
    {
        $input = new TransactionInput('TEST', 'invalid', 2);

        $security = $this->createMock(Security::class);
        $security->expects($this->once())
            ->method('getUser')
            ->willReturn(new User());
        $coinRepository = $this->createMock(CoinRepository::class);
        $transactionFactory = $this->createMock(TransactionFactoryInterface::class);

        $transformer = new TransactionInputDataTransformer($security, $coinRepository, $transactionFactory);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid type, use buy or sell');

        $transformer->transform($input, Transaction::class);
    }

    public function testTransformNonPositiveQuantity(): void
    {
        $input = new TransactionInput('TEST', TransactionType::Buy->value, 0);

        $security = $this->createMock(Security::class);
        $security->expects($this->once())
            ->method('getUser')
            ->willReturn(new User());
        $coinRepository = $this->createMock(CoinRepository::class);
        $transactionFactory = $this->createMock(TransactionFactoryInterface::class);

        $transformer = new TransactionInputDataTransformer($security, $coinRepository, $transactionFactory);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Quantity must be positive');

        $transformer->transform($input, Transaction::class);
    }

    public function testTransformInvalidCoinCode(): void
    {
        $input = new TransactionInput('TEST', TransactionType::Buy->value, 2);

        $security = $this->createMock(Security::class);
        $security->expects($this->once())
            ->method('getUser')
            ->willReturn(new User());
        $coinRepository = $this->createMock(CoinRepository::class);
        $coinRepository->expects($this->once())
            ->method('findOneBy')
            ->with(['code' => $input->coinCode])
            ->willReturn(null);
        $transactionFactory = $this->createMock(TransactionFactoryInterface::class);

        $transformer = new TransactionInputDataTransformer($security, $coinRepository, $transactionFactory);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid coin code');

        $transformer->transform($input, Transaction::class);
    }

    public function testSupportsTransformation(): void
    {
        $security = $this->createMock(Security::class);
        $coinRepository = $this->createMock(CoinRepository::class);
        $transactionFactory = $this->createMock(TransactionFactoryInterface::class);

        $transformer = new TransactionInputDataTransformer($security, $coinRepository, $transactionFactory);

        $this->assertTrue($transformer->supportsTransformation(new \stdClass(), Transaction::class, ['input' => ['class' => TransactionInput::class]]));
        $this->assertFalse($transformer->supportsTransformation(new \stdClass(), 'TEST', ['input' => ['class' => TransactionInput::class]]));
        $this->assertFalse($transformer->supportsTransformation(new \stdClass(), Transaction::class, []));

        $user = new User();
        $coin = new Coin('TEST', 'TEST', 1);
        $type = TransactionType::Buy;
        $quantity = 2;
        $transaction = new Transaction($user, $coin, $type, $quantity, 0, 0);
        $this->assertFalse($transformer->supportsTransformation($transaction, Transaction::class, ['input' => ['class' => TransactionInput::class]]));
    }
}
