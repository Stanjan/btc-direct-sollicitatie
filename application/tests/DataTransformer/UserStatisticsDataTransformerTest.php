<?php

namespace App\Tests\DataTransformer;

use App\DataTransformer\UserStatisticsDataTransformer;
use App\Dto\UserStatistics;
use App\Entity\User;
use App\Statistics\UserStatisticsFactoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\DataTransformer\UserStatisticsDataTransformer
 * @covers \App\Dto\UserStatistics
 */
class UserStatisticsDataTransformerTest extends TestCase
{
    public function testTransform(): void
    {
        $user = new User();
        $statistics = new UserStatistics('test', 1, 2, 3, []);

        $userStatisticsFactory = $this->createMock(UserStatisticsFactoryInterface::class);
        $userStatisticsFactory->expects($this->once())
            ->method('create')
            ->with($user)
            ->willReturn($statistics);

        $transformer = new UserStatisticsDataTransformer($userStatisticsFactory);

        $transformedUser = $transformer->transform($user, UserStatistics::class);

        $this->assertSame($statistics, $transformedUser);
    }

    public function testSupportsTransformation(): void
    {
        $userStatisticsFactory = $this->createMock(UserStatisticsFactoryInterface::class);
        $user = new User();

        $transformer = new UserStatisticsDataTransformer($userStatisticsFactory);

        $this->assertTrue($transformer->supportsTransformation($user, UserStatistics::class));
        $this->assertFalse($transformer->supportsTransformation(new \stdClass(), UserStatistics::class));
        $this->assertFalse($transformer->supportsTransformation($user, 'test'));
    }
}
