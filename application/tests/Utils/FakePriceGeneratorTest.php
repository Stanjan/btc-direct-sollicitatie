<?php

namespace App\Tests\Utils;

use App\Utils\FakePriceGenerator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Utils\FakePriceGenerator
 */
class FakePriceGeneratorTest extends TestCase
{
    public function testGenerateWithoutBasePrice(): void
    {
        $price = FakePriceGenerator::generate();

        // Make sure the price is between the min and max price.
        $this->assertGreaterThanOrEqual(FakePriceGenerator::MIN_PRICE, $price);
        $this->assertLessThanOrEqual(FakePriceGenerator::MAX_PRICE, $price);
    }

    public function testGenerateWithBasePrice(): void
    {
        $basePrice = 500;
        $price = FakePriceGenerator::generate($basePrice);

        // Make sure the price does not exceed the multiplier.
        $this->assertGreaterThanOrEqual($basePrice * (1 - FakePriceGenerator::MAX_DIFFERENCE_MULTIPLIER), $price);
        $this->assertLessThanOrEqual($basePrice * (1 + FakePriceGenerator::MAX_DIFFERENCE_MULTIPLIER), $price);
    }

    public function testGenerateWithNegativeBasePrice(): void
    {
        $price = FakePriceGenerator::generate(-100_000);

        // Make sure the price is always positive.
        $this->assertGreaterThanOrEqual(0, $price);
    }
}
