<?php

namespace App\Tests\Transaction;

use App\Entity\Coin;
use App\Entity\User;
use App\Transaction\TransactionFactory;
use App\Transaction\TransactionFeeCalculatorInterface;
use App\Transaction\TransactionType;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Transaction\TransactionFactory
 */
class TransactionFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $coin = new Coin('TEST', 'TEST', 100_000);
        $user = new User();
        $quantity = 0.5;
        $date = new \DateTimeImmutable('2011-11-11 11:11');
        $transactionType = TransactionType::Buy;
        $fee = 0.50;

        $feeCalculator = $this->createMock(TransactionFeeCalculatorInterface::class);
        $feeCalculator->expects($this->once())
            ->method('calculate')
            ->with($coin, $user, $quantity)
            ->willReturn($fee);

        $factory = new TransactionFactory($feeCalculator);

        $transaction = $factory->create($coin, $user, $transactionType, $quantity, $date);

        $this->assertSame($coin, $transaction->getCoin());
        $this->assertSame($user, $transaction->getUser());
        $this->assertEquals($coin->getPrice(), $transaction->getCoinPrice());
        $this->assertEquals($date, $transaction->getDate());
        $this->assertEquals($quantity, $transaction->getQuantity());
        $this->assertSame($transactionType, $transaction->getType());
        $this->assertEquals($fee, $transaction->getFee());
    }

    public function testCreateWithoutDate(): void
    {
        $coin = new Coin('TEST', 'TEST', 100_000);
        $user = new User();
        $quantity = 1;
        $feeCalculator = $this->createMock(TransactionFeeCalculatorInterface::class);
        $feeCalculator->expects($this->once())
            ->method('calculate')
            ->with($coin, $user, $quantity)
            ->willReturn(1.00);

        $factory = new TransactionFactory($feeCalculator);

        $transaction = $factory->create($coin, $user, TransactionType::Buy, 1);

        $today = new \DateTimeImmutable();
        $this->assertEquals($today->getTimestamp(), $transaction->getDate()->getTimestamp());
    }

    public function testCreateTypes(): void
    {
        $coin = new Coin('TEST', 'TEST', 100_000);
        $user = new User();
        $quantity = 1;
        $feeCalculator = $this->createMock(TransactionFeeCalculatorInterface::class);
        $feeCalculator->expects($this->exactly(count(TransactionType::cases())))
            ->method('calculate')
            ->with($coin, $user, $quantity)
            ->willReturn(1.00);

        $factory = new TransactionFactory($feeCalculator);

        // Create a transaction for every TransactionType.
        foreach (TransactionType::cases() as $type) {
            $transaction = $factory->create($coin, $user, $type, $quantity);

            $this->assertSame($type, $transaction->getType());
        }
    }
}
