<?php

namespace App\Tests\Transaction;

use App\Entity\Coin;
use App\Entity\User;
use App\Transaction\TransactionFeeCalculator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Transaction\TransactionFeeCalculator
 */
class TransactionFeeCalculatorTest extends TestCase
{
    public function testCalculate(): void
    {
        $calculator = new TransactionFeeCalculator();

        $coin = new Coin('TEST', 'TEST', 100);
        $coin->setFixedFee(1.11);
        $coin->setPercentageFee(5);
        $user = new User();
        $quantity = 1;

        $fee = $calculator->calculate($coin, $user, $quantity);

        $this->assertEquals(1.11 + $coin->getPrice() * 0.05, $fee);
    }

    public function testCalculateMultiple(): void
    {
        $calculator = new TransactionFeeCalculator();

        $coin = new Coin('TEST', 'TEST', 100);
        $coin->setFixedFee(1.11);
        $coin->setPercentageFee(5);
        $user = new User();
        $quantity = 5;

        $fee = $calculator->calculate($coin, $user, $quantity);

        $this->assertEquals(1.11 + $coin->getPrice() * 0.05 * 5, $fee);
    }

    public function testCalculateCoinPrice(): void
    {
        $calculator = new TransactionFeeCalculator();

        $coin = new Coin('TEST', 'TEST', 100);
        $coin->setFixedFee(1.11);
        $coin->setPercentageFee(5);
        $user = new User();
        $quantity = 1;

        $fee = $calculator->calculate($coin, $user, $quantity, 100_000);

        $this->assertEquals(1.11 + 100_000 * 0.05, $fee);
    }

    public function testCalculateNoFee(): void
    {
        $calculator = new TransactionFeeCalculator();

        $coin = new Coin('TEST', 'TEST', 100);
        $user = new User();
        $quantity = 1;

        $fee = $calculator->calculate($coin, $user, $quantity);

        $this->assertEquals(0, $fee);
    }
}
