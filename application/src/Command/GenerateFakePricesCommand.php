<?php

namespace App\Command;

use App\Entity\CoinPrice;
use App\Repository\CoinRepository;
use App\Utils\FakePriceGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:generate-fake-prices',
    description: 'Generates a new fake price for all coins',
)]
class GenerateFakePricesCommand extends Command
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private CoinRepository $coinRepository,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $coins = $this->coinRepository->findAll();
        foreach ($coins as $coin) {
            $newPrice = FakePriceGenerator::generate($coin->getPrice());
            $coin->setPrice($newPrice);
            $this->entityManager->persist($coin);

            // Make sure the price is also created in the coin price history.
            $coinPrice = new CoinPrice($coin, $newPrice);
            $this->entityManager->persist($coinPrice);
        }

        $this->entityManager->flush();

        $io->success('Generated new fake prices for all coins.');

        return Command::SUCCESS;
    }
}
