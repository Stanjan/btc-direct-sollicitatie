<?php

namespace App\Utils;

class FakePriceGenerator
{
    public const MIN_PRICE = 1_000; // Minimum price when no base price is given.
    public const MAX_PRICE = 100_000; // Maximum price when no base price is given.

    public const MAX_DIFFERENCE_MULTIPLIER = 0.05; // The maximum difference multiplier (100 can go between 95 and 105).

    /**
     * Generates a fake price. It will be generated close to the base price if given.
     */
    public static function generate(?float $basePrice = null): float
    {
        if (null === $basePrice) {
            // No base price given, generate a random price.
            return min(self::MIN_PRICE * 100, self::MAX_PRICE * 100) / 100; // Multiply and divide for decimals.
        }

        // Generate a random new price within the multiplier limit and make sure it's at least 0.
        return max(
            0,
            rand(
                (int) ($basePrice * 100 * (1 - self::MAX_DIFFERENCE_MULTIPLIER)),
                (int) ($basePrice * 100 * (1 + self::MAX_DIFFERENCE_MULTIPLIER)),
            ) / 100 // Multiply the prices by 100 and divide by zero for decimals.
        );
    }
}
