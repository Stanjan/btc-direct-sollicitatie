<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\Parameter;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\OpenApi;

/**
 * Removes the identifier requirement from the OpenApi docs from endpoints that do not need it (for example the /users/me endpoint).
 */
class OpenApiFactory implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {
    }

    /**
     * @param array<mixed> $context
     */
    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

        foreach ($openApi->getPaths()->getPaths() as $path => $pathItem) {
            // remove identifier parameter from operations which include "#withoutIdentifier" in the description
            foreach (PathItem::$methods as $method) {
                $getter = 'get'.ucfirst(strtolower($method));
                $setter = 'with'.ucfirst(strtolower($method));
                /** @var ?Operation $operation */
                $operation = $pathItem->$getter();

                if ($operation && str_contains($operation->getSummary(), '#withoutIdentifier')) {
                    /** @var Parameter[] $parameters */
                    $parameters = $operation->getParameters();
                    foreach ($parameters as $i => $parameter) {
                        if (preg_match('/identifier/i', $parameter->getDescription())) {
                            unset($parameters[$i]);
                            break;
                        }
                    }

                    $description = str_replace('#withoutIdentifier', '', $operation->getSummary());
                    $openApi->getPaths()->addPath($path, $pathItem = $pathItem->$setter($operation->withSummary($description)->withParameters(array_values($parameters))));
                }
            }
        }

        return $openApi;
    }
}
