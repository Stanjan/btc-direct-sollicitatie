<?php

namespace App\Transaction;

enum TransactionType: string
{
    case Sell = 'sell';
    case Buy = 'buy';
}
