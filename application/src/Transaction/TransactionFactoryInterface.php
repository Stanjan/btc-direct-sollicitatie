<?php

namespace App\Transaction;

use App\Entity\Coin;
use App\Entity\Transaction;
use App\Entity\User;

interface TransactionFactoryInterface
{
    /**
     * Creates a transaction for the current coin prices for the given user, quantity and type.
     */
    public function create(Coin $coin, User $user, TransactionType $type, float $quantity, ?\DateTimeImmutable $date = null): Transaction;
}
