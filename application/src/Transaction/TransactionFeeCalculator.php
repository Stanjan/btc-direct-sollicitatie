<?php

namespace App\Transaction;

use App\Entity\Coin;
use App\Entity\User;

class TransactionFeeCalculator implements TransactionFeeCalculatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function calculate(Coin $coin, ?User $user, float $quantity, ?float $coinPrice = null): float
    {
        if (null === $coinPrice) {
            $coinPrice = $coin->getPrice();
        }

        // Calculate both fee types before applying them.
        $fixedFee = $coin->getFixedFee();
        $percentageFee = round(($coin->getPercentageFee() / 100) * $coinPrice * $quantity, 2); // Round fee to two decimals.

        return $fixedFee + $percentageFee;
    }
}
