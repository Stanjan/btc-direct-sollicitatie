<?php

namespace App\Transaction;

use App\Entity\Coin;
use App\Entity\User;

interface TransactionFeeCalculatorInterface
{
    /**
     * Calculates the fee for the given coin, user and quantity.
     *
     * @param ?float $coinPrice if no price is given, the current coin price will be used
     */
    public function calculate(Coin $coin, ?User $user, float $quantity, ?float $coinPrice = null): float;
}
