<?php

namespace App\Transaction;

use App\Entity\Coin;
use App\Entity\Transaction;
use App\Entity\User;

class TransactionFactory implements TransactionFactoryInterface
{
    public function __construct(
        private TransactionFeeCalculatorInterface $feeCalculator,
    ) {
    }

    public function create(Coin $coin, User $user, TransactionType $type, float $quantity, ?\DateTimeImmutable $date = null): Transaction
    {
        $price = round($coin->getPrice() * $quantity, 2);
        $fee = $this->feeCalculator->calculate($coin, $user, $quantity);
        $total = $price + $fee;

        return new Transaction($user, $coin, $type, $quantity, $fee, $total, $date);
    }
}
