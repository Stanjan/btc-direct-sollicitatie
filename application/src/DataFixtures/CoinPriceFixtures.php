<?php

namespace App\DataFixtures;

use App\Entity\Coin;
use App\Entity\CoinPrice;
use App\Utils\FakePriceGenerator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CoinPriceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var Coin[] $coins */
        $coins = [
            $this->getReference(CoinFixtures::BTC_REFERENCE),
            $this->getReference(CoinFixtures::ETH_REFERENCE),
        ];

        // Generate the date period to generate prices for.
        $fromDate = new \DateTimeImmutable('-10 days');
        $toDate = new \DateTimeImmutable();
        $dateInterval = \DateInterval::createFromDateString('1 day');
        $datePeriod = new \DatePeriod($fromDate, $dateInterval, $toDate);

        // Make sure to reverse the dates so that the base price will count back instead of up.
        $dates = array_reverse(iterator_to_array($datePeriod));

        foreach ($coins as $coin) {
            // Generate history prices for the dates.
            $basePrice = $coin->getPrice();
            foreach ($dates as $date) {
                $basePrice = FakePriceGenerator::generate($basePrice);
                $manager->persist(new CoinPrice($coin, $basePrice, $date));
            }

            // Make sure the last price is the current price.
            $manager->persist(new CoinPrice($coin, $coin->getPrice()));
        }

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            CoinFixtures::class,
        ];
    }
}
