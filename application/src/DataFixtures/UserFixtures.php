<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public const DEFAULT_USER_REFERENCE = 'default-user';
    public const FAKE_USER_REFERENCE = 'fake-user';

    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $defaultUser = new User();
        $defaultUser->setEmail('foo@bar.com');
        $password = $this->userPasswordHasher->hashPassword($defaultUser, 'password');
        $defaultUser->setPassword($password);

        $manager->persist($defaultUser);
        $manager->flush();

        $this->addReference(self::DEFAULT_USER_REFERENCE, $defaultUser);

        $fakeUser = new User();
        $fakeUser->setEmail('bar@foo.com');
        $password = $this->userPasswordHasher->hashPassword($fakeUser, 'password');
        $fakeUser->setPassword($password);

        $manager->persist($fakeUser);
        $manager->flush();

        $this->addReference(self::FAKE_USER_REFERENCE, $fakeUser);
    }
}
