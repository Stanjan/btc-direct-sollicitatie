<?php

namespace App\DataFixtures;

use App\Entity\Coin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CoinFixtures extends Fixture
{
    public const BTC_REFERENCE = 'btc-coin';
    public const ETH_REFERENCE = 'eth-coin';

    public function load(ObjectManager $manager): void
    {
        $btc = new Coin('BTC', 'Bitcoin', 40_000);
        $btc->setFixedFee(3.50);
        $btc->setPercentageFee(3.5);
        $manager->persist($btc);
        $this->addReference(self::BTC_REFERENCE, $btc);

        $eth = new Coin('ETH', 'Ethereum', 2_500);
        $eth->setFixedFee(1.50);
        $eth->setPercentageFee(5);
        $manager->persist($eth);
        $this->addReference(self::ETH_REFERENCE, $eth);

        $manager->flush();
    }
}
