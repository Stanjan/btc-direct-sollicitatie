<?php

namespace App\DataFixtures;

use App\Entity\Coin;
use App\Entity\User;
use App\Transaction\TransactionFactory;
use App\Transaction\TransactionType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TransactionFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private TransactionFactory $transactionFactory,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        /** @var Coin[] $coins */
        $coins = [
            $this->getReference(CoinFixtures::BTC_REFERENCE),
            $this->getReference(CoinFixtures::ETH_REFERENCE),
        ];

        /** @var User $user */
        $user = $this->getReference(UserFixtures::DEFAULT_USER_REFERENCE);

        foreach ($coins as $coin) {
            // Create a buy and sell transaction for every coin.
            $buyTransaction = $this->transactionFactory->create($coin, $user, TransactionType::Buy, 1);
            $sellTransaction = $this->transactionFactory->create($coin, $user, TransactionType::Sell, 1);

            $manager->persist($buyTransaction);
            $manager->persist($sellTransaction);
        }

        // Create another transaction for the test user for security testing.
        /** @var User $fakeUser */
        $fakeUser = $this->getReference(UserFixtures::FAKE_USER_REFERENCE);
        $fakeTransaction = $this->transactionFactory->create($coins[0], $fakeUser, TransactionType::Buy, 1);
        $manager->persist($fakeTransaction);

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            CoinFixtures::class,
            UserFixtures::class,
        ];
    }
}
