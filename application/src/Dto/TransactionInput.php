<?php

namespace App\Dto;

class TransactionInput
{
    public function __construct(
        public string $coinCode,
        public string $type,
        public float $quantity,
    ) {
    }
}
