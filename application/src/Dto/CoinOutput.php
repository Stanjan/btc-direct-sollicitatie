<?php

namespace App\Dto;

use App\Entity\Coin;

class CoinOutput
{
    public string $code;
    public string $name;
    public float $price;

    public function __construct(
        Coin $coin,
        public float $buyPrice,
        public float $sellPrice,
    ) {
        $this->code = $coin->getCode();
        $this->name = $coin->getName();
        $this->price = $coin->getPrice();
    }
}
