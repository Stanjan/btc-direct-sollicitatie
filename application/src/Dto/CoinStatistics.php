<?php

namespace App\Dto;

class CoinStatistics
{
    public function __construct(
        public string $code,
        public float $quantityBought,
        public float $quantitySold,
        public float $averageBuyPrice,
        public float $averageSellPrice,
        public float $totalProfit,
        public int $totalBuyTransactions,
        public int $totalSellTransactions,
    ) {
    }
}
