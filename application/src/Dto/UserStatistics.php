<?php

namespace App\Dto;

class UserStatistics
{
    public function __construct(
        public string $username,
        public float $totalProfit,
        public int $totalBuyTransactions,
        public int $totalSellTransactions,
        /** @var CoinStatistics[] $coinStatistics */
        public array $coinStatistics,
    ) {
    }
}
