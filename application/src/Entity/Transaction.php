<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\TransactionInput;
use App\Repository\TransactionRepository;
use App\Transaction\TransactionType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: TransactionRepository::class)]
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get'],
    attributes: ['security' => 'is_granted("ROLE_USER")'],
    input: TransactionInput::class,
    normalizationContext: ['groups' => [self::NORMALIZATION_CONTEXT_READ]],
    order: ['date' => 'DESC'],
)]
class Transaction implements BelongsToUserInterface
{
    public const NORMALIZATION_CONTEXT_READ = 'read';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(identifier: false)]
    private int $id;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true)]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private string $token;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $user;

    #[ORM\ManyToOne(targetEntity: Coin::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Coin $coin;

    #[ORM\Column(type: 'string', length: 4)]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private string $type;

    /**
     * Coin price at the moment of the transaction.
     */
    #[ORM\Column(type: 'decimal', precision: 9, scale: 2)]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private float $coinPrice;

    #[ORM\Column(type: 'decimal', precision: 15, scale: 8)]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private float $quantity;

    #[ORM\Column(type: 'decimal', precision: 9, scale: 2)]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private float $fee;

    /**
     * Total amount of EUR including the fee.
     */
    #[ORM\Column(type: 'decimal', precision: 11, scale: 2)]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private float $total;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private \DateTimeImmutable $date;

    public function __construct(
        User $user,
        Coin $coin,
        TransactionType $type,
        float $quantity,
        float $fee,
        float $total,
        ?\DateTimeImmutable $date = null,
    ) {
        $this->user = $user;
        $this->coin = $coin;
        $this->type = $type->value;
        $this->quantity = $quantity;
        $this->fee = $fee;
        $this->total = $total;
        $this->date = $date ?: new \DateTimeImmutable();

        $this->token = Uuid::v6();
        $this->coinPrice = $coin->getPrice();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getCoin(): Coin
    {
        return $this->coin;
    }

    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    public function getCoinCode(): string
    {
        return $this->coin->getCode();
    }

    public function getType(): TransactionType
    {
        return TransactionType::from($this->type);
    }

    public function getCoinPrice(): float
    {
        return $this->coinPrice;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function getFee(): float
    {
        return $this->fee;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }
}
