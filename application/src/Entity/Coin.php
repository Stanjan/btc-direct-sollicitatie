<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Dto\CoinOutput;
use App\Repository\CoinRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CoinRepository::class)]
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    attributes: ['pagination_enabled' => false],
    output: CoinOutput::class,
)]
class Coin
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(identifier: false)]
    private int $id;

    #[ORM\Column(type: 'string', length: 5, unique: true)]
    #[ApiProperty(identifier: true)]
    private string $code;

    #[ORM\Column(type: 'string', length: 20)]
    private string $name;

    #[ORM\Column(type: 'decimal', precision: 9, scale: 2)]
    private float $price;

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $lastUpdatedAt;

    /**
     * In actual percentages, so ten percent will be 10.00.
     */
    #[ORM\Column(type: 'decimal', precision: 5, scale: 3)]
    private float $percentageFee = 0;

    #[ORM\Column(type: 'decimal', precision: 6, scale: 2)]
    private float $fixedFee = 0;

    /**
     * @var Collection<int, CoinPrice>
     */
    #[ORM\OneToMany(mappedBy: 'coin', targetEntity: CoinPrice::class, orphanRemoval: true)]
    #[ApiSubresource]
    private Collection $prices;

    public function __construct(string $code, string $name, float $price)
    {
        $this->code = $code;
        $this->name = $name;
        $this->price = $price;

        $this->lastUpdatedAt = new \DateTimeImmutable();
        $this->prices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        // Always update the lastUpdatedAt when updating the price.
        $this->lastUpdatedAt = new \DateTimeImmutable();

        return $this;
    }

    public function getLastUpdatedAt(): \DateTimeImmutable
    {
        return $this->lastUpdatedAt;
    }

    public function setLastUpdatedAt(\DateTimeImmutable $lastUpdatedAt): self
    {
        $this->lastUpdatedAt = $lastUpdatedAt;

        return $this;
    }

    public function getPercentageFee(): float
    {
        return $this->percentageFee;
    }

    public function setPercentageFee(float $percentageFee): self
    {
        $this->percentageFee = $percentageFee;

        return $this;
    }

    public function getFixedFee(): float
    {
        return $this->fixedFee;
    }

    public function setFixedFee(float $fixedFee): self
    {
        $this->fixedFee = $fixedFee;

        return $this;
    }

    /**
     * @return Collection<int, CoinPrice>
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(CoinPrice $price): self
    {
        if (!$this->prices->contains($price)) {
            $this->prices[] = $price;
        }

        return $this;
    }

    public function removePrice(CoinPrice $price): self
    {
        $this->prices->removeElement($price);

        return $this;
    }
}
