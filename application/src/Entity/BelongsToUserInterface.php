<?php

namespace App\Entity;

interface BelongsToUserInterface
{
    public function getUser(): User;
}
