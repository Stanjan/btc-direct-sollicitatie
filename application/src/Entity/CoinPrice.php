<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CoinPriceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: CoinPriceRepository::class)]
#[ApiResource(
    collectionOperations: [],
    itemOperations: ['get'],
    normalizationContext: ['groups' => [self::NORMALIZATION_CONTEXT_READ]],
    order: ['date' => 'DESC'],
)]
class CoinPrice
{
    public const NORMALIZATION_CONTEXT_READ = 'read';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(identifier: false)]
    private int $id;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true)]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private string $token;

    #[ORM\ManyToOne(targetEntity: Coin::class, inversedBy: 'prices')]
    #[ORM\JoinColumn(nullable: false)]
    private Coin $coin;

    #[ORM\Column(type: 'decimal', precision: 9, scale: 2)]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private float $price;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    private \DateTimeImmutable $date;

    public function __construct(Coin $coin, float $price, \DateTimeImmutable $date = new \DateTimeImmutable())
    {
        $this->coin = $coin;
        $this->price = $price;
        $this->date = $date;

        $this->token = Uuid::v6();

        $coin->addPrice($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getCoin(): Coin
    {
        return $this->coin;
    }

    #[Groups(self::NORMALIZATION_CONTEXT_READ)]
    public function getCoinCode(): string
    {
        return $this->coin->getCode();
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }
}
