<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\User;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;

/**
 * The get endpoint is replaced with "me" instead of the id, it will always return the information of the authenticated user.
 */
class UserDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    public function __construct(
        private Security $security,
    ) {
    }

    /**
     * @param array<mixed> $context
     * @param int|string   $id
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): User
    {
        $user = $this->security->getUser();
        if (!$user instanceof User) {
            throw new AccessDeniedHttpException();
        }

        return $user;
    }

    /**
     * @param array<mixed> $context
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return 'get' === $operationName && User::class === $resourceClass;
    }
}
