<?php

namespace App\Statistics;

use App\Dto\UserStatistics;
use App\Entity\User;

interface UserStatisticsFactoryInterface
{
    public function create(User $user): UserStatistics;
}
