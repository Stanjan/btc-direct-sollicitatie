<?php

namespace App\Statistics;

use App\Dto\CoinStatistics;
use App\Entity\Coin;
use App\Entity\User;

interface CoinStatisticsFactoryInterface
{
    public function create(Coin $coin, ?User $user = null): CoinStatistics;

    /**
     * @return CoinStatistics[]
     */
    public function createAll(?User $user = null): array;
}
