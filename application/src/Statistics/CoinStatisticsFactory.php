<?php

namespace App\Statistics;

use App\Dto\CoinStatistics;
use App\Entity\Coin;
use App\Entity\User;
use App\Repository\TransactionRepository;
use App\Transaction\TransactionType;
use Doctrine\ORM\QueryBuilder;

class CoinStatisticsFactory implements CoinStatisticsFactoryInterface
{
    public function __construct(
        private TransactionRepository $transactionRepository,
    ) {
    }

    /**
     * Creates the statistics for one coin.
     * If a user is given, only the statistics of that user will be used.
     */
    public function create(Coin $coin, ?User $user = null): CoinStatistics
    {
        $data = $this->createQueryBuilder($user)
            ->andWhere('c.id = :coinId')
            ->setParameter('coinId', $coin->getId())
            ->getQuery()
            ->setMaxResults(1)
            ->getSingleResult();

        return $this->createStatisticsForData($data);
    }

    /**
     * Creates the statistics for all coins.
     * If a user is given, only the statistics of that user will be used.
     */
    public function createAll(?User $user = null): array
    {
        $data = $this->createQueryBuilder($user)
            ->groupBy('c.id')
            ->getQuery()
            ->getResult();

        $statistics = [];
        foreach ($data as $coinData) {
            $statistics[] = $this->createStatisticsForData($coinData);
        }

        return $statistics;
    }

    private function createQueryBuilder(?User $user): QueryBuilder
    {
        $qb = $this->transactionRepository->createQueryBuilder('t')
            ->select('c.code AS code'
                .', SUM(CASE WHEN t.type = :buyType THEN t.quantity ELSE 0 END) AS quantity_bought'
                .', SUM(CASE WHEN t.type = :sellType THEN t.quantity ELSE 0 END) AS quantity_sold'
                .', SUM(CASE WHEN t.type = :buyType THEN t.total ELSE 0 END) AS buy_total'
                .', SUM(CASE WHEN t.type = :sellType THEN t.total ELSE 0 END) AS sell_total'
                .', SUM(CASE WHEN t.type = :buyType THEN 1 ELSE 0 END) AS total_buy_transactions'
                .', SUM(CASE WHEN t.type = :sellType THEN 1 ELSE 0 END) AS total_sell_transactions'
            )
            ->setParameter('buyType', TransactionType::Buy->value)
            ->setParameter('sellType', TransactionType::Sell->value)
            ->leftJoin('t.coin', 'c');

        if ($user) {
            $qb->where('IDENTITY(t.user) = :userId')
                ->setParameter('userId', $user->getId());
        }

        return $qb;
    }

    /**
     * @param array<mixed> $data
     */
    private function createStatisticsForData(array $data): CoinStatistics
    {
        $code = $data['code'];
        $quantityBought = (float) $data['quantity_bought'];
        $quantitySold = (float) $data['quantity_sold'];
        $buyTotal = (float) $data['buy_total'];
        $sellTotal = (float) $data['sell_total'];
        $averageBuyPrice = round($buyTotal / $quantityBought, 2);
        $averageSellPrice = round($sellTotal / $quantitySold, 2);
        $totalProfit = $sellTotal - $buyTotal;
        $totalBuyTransactions = (int) $data['total_buy_transactions'];
        $totalSellTransactions = (int) $data['total_sell_transactions'];

        return new CoinStatistics(
            $code,
            $quantityBought,
            $quantitySold,
            $averageBuyPrice,
            $averageSellPrice,
            $totalProfit,
            $totalBuyTransactions,
            $totalSellTransactions
        );
    }
}
