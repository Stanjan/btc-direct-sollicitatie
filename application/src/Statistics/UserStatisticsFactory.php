<?php

namespace App\Statistics;

use App\Dto\UserStatistics;
use App\Entity\User;
use App\Repository\TransactionRepository;
use App\Transaction\TransactionType;

class UserStatisticsFactory implements UserStatisticsFactoryInterface
{
    public function __construct(
        private TransactionRepository $transactionRepository,
        private CoinStatisticsFactoryInterface $coinStatisticsFactory,
    ) {
    }

    public function create(User $user): UserStatistics
    {
        $data = $this->transactionRepository->createQueryBuilder('t')
            ->select('SUM(CASE WHEN t.type = :buyType THEN t.total ELSE 0 END) AS buy_total'
                .', SUM(CASE WHEN t.type = :sellType THEN t.total ELSE 0 END) AS sell_total'
                .', SUM(CASE WHEN t.type = :buyType THEN 1 ELSE 0 END) AS total_buy_transactions'
                .', SUM(CASE WHEN t.type = :sellType THEN 1 ELSE 0 END) AS total_sell_transactions'
            )
            ->where('IDENTITY(t.user) = :userId')
            ->setParameter('userId', $user->getId())
            ->setParameter('buyType', TransactionType::Buy->value)
            ->setParameter('sellType', TransactionType::Sell->value)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();

        $buyTotal = (float) $data['buy_total'];
        $sellTotal = (float) $data['sell_total'];
        $totalProfit = $sellTotal - $buyTotal;
        $totalBuyTransactions = (int) $data['total_buy_transactions'];
        $totalSellTransactions = (int) $data['total_sell_transactions'];

        return new UserStatistics(
            $user->getUsername(),
            $totalProfit,
            $totalBuyTransactions,
            $totalSellTransactions,
            $this->coinStatisticsFactory->createAll($user),
        );
    }
}
