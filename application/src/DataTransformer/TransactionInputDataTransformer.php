<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\TransactionInput;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\CoinRepository;
use App\Transaction\TransactionFactoryInterface;
use App\Transaction\TransactionType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

class TransactionInputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private Security $security,
        private CoinRepository $coinRepository,
        private TransactionFactoryInterface $transactionFactory,
    ) {
    }

    /**
     * @param TransactionInput $object
     * @param array<mixed>     $context
     */
    public function transform($object, string $to, array $context = []): Transaction
    {
        // Calculate the fee for one coin for the logged in user.
        $user = $this->security->getUser();
        if (!$user instanceof User) {
            throw new AccessDeniedHttpException('User must be an instance of \App\Entity\User');
        }

        $type = TransactionType::tryFrom($object->type);
        if (null === $type) {
            throw new InvalidArgumentException('Invalid type, use buy or sell');
        }

        $quantity = $object->quantity;
        if ($quantity <= 0) {
            throw new InvalidArgumentException('Quantity must be positive');
        }

        $coin = $this->coinRepository->findOneBy(['code' => $object->coinCode]);
        if (!$coin) {
            throw new InvalidArgumentException('Invalid coin code');
        }

        return $this->transactionFactory->create($coin, $user, $type, $quantity);
    }

    /**
     * @param object       $data
     * @param array<mixed> $context
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Transaction) {
            return false;
        }

        return Transaction::class === $to && TransactionInput::class === ($context['input']['class'] ?? null);
    }
}
