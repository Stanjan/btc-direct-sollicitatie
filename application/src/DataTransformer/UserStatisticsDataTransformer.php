<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\UserStatistics;
use App\Entity\User;
use App\Statistics\UserStatisticsFactoryInterface;

class UserStatisticsDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private UserStatisticsFactoryInterface $userStatisticsFactory,
    ) {
    }

    /**
     * @param User         $object
     * @param array<mixed> $context
     */
    public function transform($object, string $to, array $context = []): UserStatistics
    {
        return $this->userStatisticsFactory->create($object);
    }

    /**
     * @param object       $data
     * @param array<mixed> $context
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return UserStatistics::class === $to && $data instanceof User;
    }
}
