<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\CoinOutput;
use App\Entity\Coin;
use App\Entity\User;
use App\Transaction\TransactionFeeCalculatorInterface;
use Symfony\Component\Security\Core\Security;

class CoinOutputDataTransformer implements DataTransformerInterface
{
    public function __construct(
        private Security $security,
        private TransactionFeeCalculatorInterface $transactionFeeCalculator,
    ) {
    }

    /**
     * @param Coin         $object
     * @param array<mixed> $context
     */
    public function transform($object, string $to, array $context = []): CoinOutput
    {
        // Calculate the fee for one coin for the logged in user.
        $user = $this->security->getUser();
        $fee = $this->transactionFeeCalculator->calculate($object, $user instanceof User ? $user : null, 1);

        $buyPrice = $object->getPrice() + $fee;
        $sellPrice = $object->getPrice() - $fee;

        return new CoinOutput($object, $buyPrice, $sellPrice);
    }

    /**
     * @param object       $data
     * @param array<mixed> $context
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return CoinOutput::class === $to && $data instanceof Coin;
    }
}
