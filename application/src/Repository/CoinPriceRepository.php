<?php

namespace App\Repository;

use App\Entity\CoinPrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CoinPrice|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoinPrice|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoinPrice[]    findAll()
 * @method CoinPrice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @extends ServiceEntityRepository<CoinPrice>
 */
class CoinPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CoinPrice::class);
    }
}
