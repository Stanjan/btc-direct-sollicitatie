# BTC Direct sollicitatie Stan Jansen

## Introduction
API for trading crypto.

Assumptions (due to lack of time):
* Your wallet has infinite coins! :tada: You can sell as many coins as you want.
* Prices are always in EUR :eu:
* Prices are always rounded
* Coin prices are fake / randomly generated
* Min. quantity per transaction is 0,00000001
* Max. quantity per transaction is 9.999.999,99999999
* Max. total per transaction is 999.999.999,99
* Max. fee per transaction is 9.999.999,99 
* The profit of transactions will be calculated on the go instead of indexed and saved to the database (where possible).
* No OpenAPI summaries, the endpoints should be self-explanatory.

PHPStan and PHP-CS-Fixer have been implemented. For my "more in-depth" open-source experience with tests, you can check a project of mine: https://gitlab.com/Stanjan/sudoku

## Setup

### 1. Docker

#### Default setup
```bash
# ./docker

docker-compose up
```

#### Traefik setup
For local development I have a personal server at home running Traefik. This project can be accessed at: https://btcdirect.stanjan.nl/api
```bash
# ./docker

docker-compose -f docker-compose.traefik.yaml up -d --build
```

### 2. Database
Reset/create the database and load the fixtures with the following script.
```bash
bin/reset-db.sh
```

### 3. JWT

https://api-platform.com/docs/core/jwt/#installing-lexikjwtauthenticationbundle

## Testing

### Lint
Install PHP-CS-Fixer using `composer install --working-dir=tools/php-cs-fixer` the first time.

```bash
# ./application

composer lint
```

### PHPUnit
```bash
# ./application

vendor/bin/phpunit tests
```

## Usage

### API platform
The API docs can be viewed at `/api`.

### Credentials
The fixtures come with the username `foo@bar.com` with password `password`.

### Generate fake prices
To generate new fake prices, use the `app:generate-fake-price` command. On my dev server this will be ran every minute via a cronjob.